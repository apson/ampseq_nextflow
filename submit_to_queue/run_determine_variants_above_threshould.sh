#! /bin/bash
#SBATCH --job-name=count3
#SBATCH -o count_reads.output
#SBATCH -e count_reads.err.output
#SBATCH --mem-per-cpu=1Gb
#SBATCH --time=00-24:00:00
#SBATCH --qos=quick
##SBATCH --partition=smrt
#SBATCH --partition=default-standard
#SBATCH --nodes=1
##SBATCH --ntasks=6
#SBATCH --cpus-per-task=1

export NXF_EXECUTOR=slurm

## input files dir is set inside the params file!
## in_data/ params_file.yaml includes parameters for the pipeline
## --project_path defualt to current directory (from where the pipeline is run)
## -resume to resume the pipeline from the last run (e.g. debugging or after a crash)


nextflow run nextflow/determine_variants_above_threshould.nf \
        --project_path "$PWD" \
        -params-file in_data/params_file.yaml\
        -resume 