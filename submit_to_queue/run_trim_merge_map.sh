#! /bin/bash
#SBATCH --job-name=fast3
#SBATCH -o fastqc.output
#SBATCH -e fastqc.output
##SBATCH --mem-per-cpu=2Gb
#SBATCH --time=00-24:00:00
#SBATCH --qos=normal
#SBATCH --partition=default-standard
#SBATCH --nodes=1
##SBATCH --ntasks=6
#SBATCH --cpus-per-task=12

export NXF_EXECUTOR=slurm

## input files dir is set inside the params file!
## in_data/ params_file.yaml includes parameters for the pipeline
## --project_path defualt to current directory (from where the pipeline is run)
## -resume to resume the pipeline from the last run (e.g. debugging or after a crash)
## --fastqcall to run FastQC for every step (slow)

nextflow run nextflow/trim_merge_map.nf \
        --project_path "$PWD" \
        -params-file in_data/params_file.yaml\
        -resume ##--fastqcall