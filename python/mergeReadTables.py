#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
from collections import Counter
import csv
import pandas as pd
import pickle
from pprint import pprint
  
parser = argparse.ArgumentParser("Combine reads counts from multiple pickle files into one frequency table as save csv")
parser.add_argument('--in-pickle', dest='pickles', metavar='FILE', type=str,nargs='+' ,
                   help='pickle files containing counts objects to be merged')

parser.add_argument('--out-csv', dest='out', help='output path for a csv file with merged reads frequency table (pickle file with the object will be exported too)',type=str, metavar='FILE')


args = parser.parse_args()

pickles=args.pickles
out=args.out


# pickles = ["here.pickle","here2.pickle" , "here3.pickle" ]

with open(pickles[0], 'rb') as inputfile:
     first_counts=pickle.load(inputfile) 
     inputfile.close()
     

for next_pickle in pickles[1:] :
    with open(next_pickle, 'rb') as inputfile: 
        next_count=pickle.load(inputfile)
        inputfile.close()
        first_counts.update(next_count)
        print( sum(first_counts.values()))
        del next_count


# convert to dictionary
result_dict={k: first_counts[k] for k in first_counts.keys() }
# transform to a table
result_table=pd.DataFrame(result_dict.items(), columns=['sequence', 'abundance'])

# ## save file

result_table.to_csv (out, index = False, header=True)

 
## save as 
with open(out.replace(".csv",".pickle"), 'wb') as outputfile:
    pickle.dump(first_counts, outputfile)
    outputfile.close()




