#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
from collections import Counter
import csv
import pickle

  
parser = argparse.ArgumentParser("Filter count dict object by threshold and return a variants to count")
parser.add_argument('--in-pickle', dest='pickle_path', metavar='FILE', type=str,
                   help='pickle file containing count object')
parser.add_argument('--threshold', dest='threshold', help='threshold to filter by',type=int, metavar='INT')
parser.add_argument('--positions', dest='positions', help='threshold to filter by',type=int,nargs='+',metavar='INT')
parser.add_argument('--out-csv', dest='out', help='output path for a csv file with list of variants to count',type=str, metavar='FILE')


args = parser.parse_args()

pickle_path=args.pickle_path
threshold =args.threshold
positions=args.positions
## shift to 0-based
positions = [x-1 for x in positions]

out=args.out

 

count_dict = pickle.load(open(pickle_path, 'rb'))

## get wt (most common) sequence

wt = count_dict.most_common(1)[0][0]

filtered = list({k for k, v in count_dict.items() if v > threshold})


## now filter by position outside of the positions list

wt = count_dict.most_common(1)[0][0]

def get_constant_string(original_string, exclude_positions):
    # Create a new string by including characters that are not in the exclude_positions
    return ''.join([char for idx, char in enumerate(original_string) if idx not in exclude_positions])

outside_seq_lst = [get_constant_string(variant, positions) for variant in filtered]

wt_outside_seq = get_constant_string(wt, positions)

filtered_only_inside = [variant for variant, outside_seq in zip(filtered, outside_seq_lst) if outside_seq == wt_outside_seq]





with open(out, 'w', newline='') as f:
    writer = csv.writer(f)
    for l in filtered_only_inside:
        writer.writerow([l])
        