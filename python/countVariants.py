#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
from collections import Counter
import csv
import pandas as pd
import pickle
  
parser = argparse.ArgumentParser("Count variants provided by user in pickle files returned by readTableFromBamToPickle.py")
parser.add_argument('--in-pickle', dest='pickle_path', metavar='FILE', type=str, help='a file with pickle object returned by readTableFromBamToPickle.py ')
parser.add_argument('--in-csv', dest='variants', help='csv file with a vector of the variants to be counted',type=str, metavar='FILE')
parser.add_argument('--out-csv', dest='out', help='output file with variant counts',type=str, metavar='FILE')


args = parser.parse_args()

pickle_path=args.pickle_path
variants=args.variants
out=args.out


# import list of variants to count
to_count=[]

print(variants)

with open(variants,'r') as csvfile:
    variant_vector = csv.reader(csvfile)
    for row in variant_vector:
        if len(row)>0:
            to_count.append(row[0])

# read bamfile 

with open(pickle_path, 'rb') as inputfile:
     reads_frequency=pickle.load(inputfile) 
     inputfile.close()
# count variants


# dictionary with the frequency of variants
result_dict={k: reads_frequency[k] for k in reads_frequency.keys() & to_count}
# transform to a table
result_table=pd.DataFrame(result_dict.items(), columns=['sequence', 'abundance'])


## save file


result_table.to_csv (out, index = False, header=True)
 




