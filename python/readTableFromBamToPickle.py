#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import pysam
from collections import Counter
import csv
import pickle
  
parser = argparse.ArgumentParser("Parse reads from a bam file and save as frequency table in a pickle object .Please note cigar filter is hardcoded")
parser.add_argument('--in-bam', dest='bamfile', metavar='FILE', type=str, 
                   help='bam file to parse')

parser.add_argument('--out-pickle', dest='out', help='output path to savel pickle object',type=str, metavar='FILE')
#parser.add_argument('--ref-name', dest='refname', help='output path to savel pickle object',type=str, metavar='STRING')


args = parser.parse_args()

bamfile = args.bamfile
#refname = args.refname
out = args.out
#ncores=int(args.ncores)

cigars=[]
refs=[]

with pysam.AlignmentFile(bamfile, "rb") as bam:
    for i, alignment in enumerate(bam):
        if i >= 100:  
            break
        cigars.append(alignment.cigarstring)
        refs.append(alignment.reference_name)

# Count frequencies of each cigar string
most_common_cigar = Counter(cigars).most_common(1)[0][0]
reference = Counter(refs).most_common(1)[0][0]

list_reads=[]
for alignment in pysam.AlignmentFile(bamfile, "rb").fetch(contig = reference):
    if(alignment.cigarstring == most_common_cigar):
        list_reads.append(alignment.seq)


## get reads counts
reads_counts=Counter(list_reads)


## save as 
with open(out, 'wb') as outputfile:
    pickle.dump(reads_counts, outputfile)
    outputfile.close()


