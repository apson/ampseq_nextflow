#!/usr/bin/env nextflow

// set input and output paths 


queue=params.queue
threshold = params.threshold
project_path = params.project_path
path_to_bam_files = project_path + "/" + params.bamdir
publish_dir = project_path + "/" + params.publish_dir
variants_that_passed = project_path + "/" + params.variants_that_passed


// set parameters of execution


/*
 *  Execution 
 */  
    

process readTableFromBamToPickle {
    
    cpus 1
    memory '8 GB'
    queue "${queue}"
    
    
    input: 
    
        set sample, path(bam) from Channel.fromPath( "${path_to_bam_files}/*.bam").map { 
            file -> 
                def sample = file.baseName
                return tuple(sample, file)
            }
        .groupTuple()
    
    output: 
         path '*.pickle' into read_tables_pickle1

    
    """
    
    python3 "${project_path}"/python/readTableFromBamToPickle.py \
    --in-bam "${path_to_bam_files}/${bam}" \
    --out-pickle ${sample}.pickle
   
    """
    
    }


process countVariants {
    
    publishDir "${publish_dir}/counts", mode: 'copy', overwrite: true
    
    cpus 1
    memory '4 GB'
    queue "${queue}"
    
    input: 
        
        set sample, path(pickle) from read_tables_pickle1.map { 
            file -> 
                def sample = file.baseName
                return tuple(sample, file)
            }
        .groupTuple()
    
    
    output:
    
        path '*.csv' into variants_counts
    
    """
    
       
    python3 "${project_path}"/python/countVariants.py \
    --in-pickle $pickle \
    --in-csv "${variants_that_passed}" \
    --out-csv  ${sample}.csv
   
    """
    
    }
    

process combineVariantCounts {
    
    publishDir "${publish_dir}", mode: 'copy', overwrite: true
    
    cpus 1
    memory '4 GB'
    queue "${queue}"
    
        
    input:
        path counts from variants_counts.collect()
    
    output:
        path '*.RDS' into final_data

    """
    
    Rscript "${project_path}"/R/combineVariantCounts.r \
            --in-csv $counts \
            --out-rds variants_counts_combined_t${threshold}.RDS
            
    """
}  

