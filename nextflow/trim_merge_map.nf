#!/usr/bin/env nextflow

// set input and output paths. several input paths are possible 

path_to_raw_data = params.dirList

project_path = params.project_path
barcodes_path = project_path + "/" + params.barcodes_path
publish_dir = project_path + "/" + params.publish_dir
ref_fasta = project_path + "/" + params.ref_fasta

// primers 
FWD = params.FWD
FWD_RC = params.FWD_RC
REV = params.REV
REV_RC = params.REV_RC

// set parameters of execution

ncores = params.n_cores

// set binaries paths

flash = params.flash
fastqc = params.fastqc

/*
 *  Execution 
 */  

// check if reference fasta file is index otherwise run bwa index

process bwa_index {
    
    cpus 1
    memory '512 MB'
    
    publishDir file("${ref_fasta}").getParent().toString() , mode: 'copy', overwrite: false
    
    input: file fasta from Channel.fromPath("${ref_fasta}")
    
    output: 
         file("*") optional true into outChannel
            
    when: !file("${ref_fasta}.sa").exists() 
    
    """
      bwa index $fasta 
   
    """
    
    
}
    
// demultiplex input files


files12 = Channel.fromFilePairs( "${path_to_raw_data}/*_{1,2}.fastq.gz" )
files21 = Channel.fromFilePairs( "${path_to_raw_data}/*_{1,2}.fastq.gz" )

process demcutadapt_round1 {
    
    cpus 24
    memory '8 GB'
    
    publishDir "${publish_dir}/demult", mode: 'copy', overwrite: true
    
    input:
        input: tuple val(x), path(reads) from files12
    
    output:
        path '*_demult_?.fastq.gz' into demultiplexed_round1, demultiplexed_round1_stats, demultiplexed_round1_qc
        
   
    shell:

    '''
            
    cutadapt -j !{ncores} -e 1 --overlap=8 --no-indels --pair-filter=first --action=trim -g file:!{barcodes_path} \
        --discard-untrimmed \
        -o {name}_!{x}_round1_demult_1.fastq.gz \
        -p {name}_!{x}_round1_demult_2.fastq.gz \
        !{reads}
    '''
} 


process demcutadapt_round2 {

    cpus 24
    memory '8 GB'
    
    publishDir "${publish_dir}/demult", mode: 'copy', overwrite: true
    
    
    input:
        input: tuple val(x), path(reads) from files21
    
    output:
        path '*_demult_?.fastq.gz' into demultiplexed_round2, demultiplexed_round2_stats, demultiplexed_round2_qc 

    shell:
    
    '''
    
   readfiles=( !{reads} ) ## convert variable to array to invert file order
    
    cutadapt -j !{ncores} -e 1 --overlap=8 --no-indels --pair-filter=first --action=trim -g file:!{barcodes_path} \
        --discard-untrimmed \
        -o {name}_!{x}_round2_demult_1.fastq.gz \
        -p {name}_!{x}_round2_demult_2.fastq.gz \
        ${readfiles[1]} ${readfiles[0]}
    '''
}  


process trimcutadapt {
    
    cpus 12
    memory '8 GB'
    
    publishDir "${publish_dir}/trim", mode: 'copy', overwrite: true
    //echo true
    input:
        set sample, file(reads) from demultiplexed_round1.concat(demultiplexed_round2).flatten().map() {
            file ->
                def sample = file.name.toString().replaceAll(/_demult_[12]\.fastq\.gz$/, '')
                return tuple(sample, file)
                }
    .groupTuple(sort:true)
    
    output:
        path '*.fastq.gz' into trimmed, trimmed_stats,trimmed_qc 
    
    
    """
   
    cutadapt -j 12 -e 0.1 --no-indels --overlap=8 --discard-untrimmed \
    -a "^${FWD}...${REV_RC};max_error_rate=0.2;min_overlap=6" \
    -A "^${REV}...${FWD_RC};max_error_rate=0.2;min_overlap=6" --pair-filter=any \
    -o '${sample}_trimmed_1.fastq.gz' \
    -p '${sample}_trimmed_2.fastq.gz' \
    --max-ee=2 -l=114 \
    $reads
    
    """
}  

// merge paired reads

process flash {
    
    publishDir "${publish_dir}/flash", mode: 'copy', overwrite: true
    cpus 24
    memory '512 MB'
    //echo true
    input:
        set sample, file(reads) from trimmed.flatten().map() {
     file ->
                def sample = file.name.toString().replaceAll(/_trimmed_[12]\.fastq\.gz$/, '')
                return tuple(sample, file)
                }
    .groupTuple()
    
    output:
        path '*.extendedFrags.fastq.gz' into merged, merged_stats,merged_qc
    
    
    """
       ${flash}  -t $ncores $reads -O -m 30 -M 140 -z \
       -o '${sample}_merged'

    """
}  



// map reads

process bwamem {
    
    cpus 12
    memory '6 GB'
    
    publishDir "${publish_dir}/bam" , mode: 'copy', overwrite: true
    //echo true
    input:
        set sample, file(reads) from merged.flatten().map() {
            file ->
                def sample = file.name.toString().replaceAll(/_merged\.extendedFrags\.fastq\.gz$/, '')
                return tuple(sample, file)
                }
    .groupTuple()
        
    
    output:
        path '*.bam' into bams, bams_stat,bam_qc
    
    
    """
      zcat $reads | bwa mem -t 12 -O 16,16 $ref_fasta - |\
      samtools sort - -o '${sample}.bam'
      
   
    """
}  

// index mapped reads
process bamindex {
    
    cpus 1
    memory '512 MB'
    
    publishDir "${publish_dir}/bam", mode: 'copy', overwrite: true
    //echo true
    input:
        path file from bams.flatten()
    
    output:
    
        path '*.bai' into bais
      
    
    """
      samtools index $file 
   
    """
}  


// make stats
process stats1 {
    
    cpus 1
    memory '256 MB'
    
    input: 
        path file from Channel.fromPath( "${path_to_raw_data}/*.fastq.gz" )
       
    output:
    
        stdout stats1
    
    """
    
    step="01_input"
    nreads=\$(zgrep "^@" "${file}"|wc -l)
    echo \$step $file \$nreads 
        
    """
    
    }


process stats2 {
    
    cpus 1
    memory '256 MB'
    
    
    input: 
        
        path file from demultiplexed_round1_stats.concat( demultiplexed_round2_stats).flatten()
   
    output:
    
        stdout stats2
    
    """
    
    step="02_demultiplex"
    nreads=\$(zgrep "^@" "${file}"|wc -l)
    echo \$step "${file}" \$nreads
        
    """
    
    
    }
    
    
process stats3 {

    cpus 1
    memory '256 MB'
    
    input: 
        
        path file from trimmed_stats.flatten()
   
    output:
    
        stdout stats3
    
    """
    
    step="03_trimmed"
    nreads=\$(zgrep "^@" "${file}"|wc -l)
    echo \$step "${file}" \$nreads 
        
    """
    
    
    }
    
    
process stats4 {
    
    cpus 1
    memory '256 MB'
    
    
    input: 
        
        path file from merged_stats.flatten()
    
    output:
    
        stdout stats4
   
    """
    
    step="04_merged"
    nreads=\$(zgrep "^@" "${file}"|wc -l)
    echo \$step "${file}" \$nreads 
        
    """
    
    
    }
    
process stats5 {
    
    cpus 1
    memory '256 MB'
    
    
    input: 
        
        path file from bams_stat.flatten()
        
    output: 
   
    stdout stats5
        
   
    """
    
    step="05_mapped"
    nreads=\$(samtools view -c -F 2308 $file)
    echo \$step "${file}" \$nreads 
        
    """
    
    
    }
    


concatenatedChannel = stats1.concat(stats2,stats3,stats4,stats5)
concatenatedChannel.collectFile(name:'stats.txt', sort: true, newLine: false,storeDir: "${publish_dir}/").set{ numbersFileChannel }


process stats_summary {

    cpus 1
    memory '256 MB'
    
    publishDir "${publish_dir}/", mode: 'copy', overwrite: true
    
    input:
    file stats from numbersFileChannel
    
    output:
    path '*.txt' into tats_summary_output
    
    script:
    """
    Rscript ${project_path}/R/pipelineStats.r $stats > stats_summary_output.txt
    """
}
  

process fastqc_raw {
    
  publishDir "${publish_dir}/raw", mode: 'copy', overwrite: true
  memory '1 GB'  
  cpus 24
  
  input:  file fastq from Channel.fromPath( "${path_to_raw_data}/*_{1,2}.fastq.gz" ).flatten()
    
  output: file "*" into out_channel_raw
  
  
  """

  ${fastqc} -t 24  ${fastq} 
  """
    
}

if (params.fastqcall) process fastqc_demult {
    
  publishDir "${publish_dir}/demult/fastqc", mode: 'copy', overwrite: true
  memory '512 MB'  
  cpus 12
  
  input:  file fastq from demultiplexed_round1_qc.concat( demultiplexed_round2_qc).flatten()
    
  output: file "*" into out_channel_qc
  
  
  """

  ${fastqc} -t 12  ${fastq}
  """
    
}
    

if (params.fastqcall) process fastqc_trim {
    
  publishDir "${publish_dir}/trim/fastqc", mode: 'copy', overwrite: true
  memory '512 MB'  
  cpus 12
  
  input:  file fastq from trimmed_qc.flatten()
    
  output: file "*" into out_channel1
  
  
  """

  ${fastqc} -t 12  ${fastq}
  """
    
}

if (params.fastqcall) process fastqc_flash {
    
  publishDir "${publish_dir}/flash/fastqc", mode: 'copy', overwrite: true
  memory '512 MB'  
  cpus 12
  
  input:  file fastq from merged_qc.flatten()
    
  output: file "*" into out_channel2
  
  
  """

  ${fastqc} -t 12  ${fastq}
  """
    
}

if (params.fastqcall) process fastqc_bam {
    
  publishDir "${publish_dir}/bam/fastq", mode: 'copy', overwrite: true
  memory '512 MB'  
  cpus 12
  
  input:  file fastq from bam_qc.flatten()
    
  output: file "*" into out_channel3
  
  
  """

  ${fastqc} -t 12 ${fastq}
  """
    
}


