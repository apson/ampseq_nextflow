#!/usr/bin/env nextflow

// set input and output paths 

project_path = params.project_path
path_to_bam_files = project_path + "/" + params.bamdir
publish_dir = project_path + "/" + params.publish_dir
queue = params.queue
threshold = params.threshold

positions = params.positions
path_to_raw_data = params.dirList



// set parameters of execution

/*
 *  Execution 
 */  


    
process readTableFromBamToPickle {
    
    cpus 1
    memory '8 GB'
    queue "${queue}"
    
    
    input: 
    
        set sample, path(bam) from Channel.fromPath( "${path_to_bam_files}/*.bam").flatten().map() {
            file ->
                def sample = file.name.toString().replaceFirst(~/\.[^\.]+$/, '')
                return tuple(sample, file)
                }
        .groupTuple()
    
    output: 
         path '*.pickle' into read_tables_pickle1

    
    """
    
    python3 "${project_path}"/python/readTableFromBamToPickle.py \
    --in-bam "${path_to_bam_files}/${bam}" \
    --out-pickle ${sample}.pickle
   
    """
    
    }


// count variants


process mergeReadTables {
    
    publishDir "${publish_dir}", mode: 'copy', overwrite: true
    cpus 1
    memory '8 GB'
    queue "${queue}"
    
    input:
        
        path pickles from read_tables_pickle1.collect()
    
    output:
        path 'merged_reads_table.csv' into merged_reads_table,merged_reads_table2
        path '*.pickle' into merged_reads_table_pickle

    """
      
    python3 "${project_path}"/python/mergeReadTables.py \
            --in-pickle $pickles \
            --out-csv 'merged_reads_table.csv'

    """
}  



process estimate_noiseR {
    
    publishDir "${publish_dir}", mode: 'copy', overwrite: true
       
    
    cpus 1
    memory '8 GB'
    queue "${queue}"
    
    input:
        
        path table from merged_reads_table2
    
    output:
        path 'noise_vs_variantR.csv' into out_noise_data2
        path 'noise_vs_variantR.pdf' into out_noise_data3
        

    """
      
    Rscript "${project_path}"/R/getNoiseVsVariantData.r \
            --in-csv "${table}" \
            --min 0  \
            --max 300 \
            --step 2 \
            --positions `echo "${positions}"` \
            --trim 0 \
            --out-csv 'noise_vs_variantR.csv'
            
  

    """
}  


process determineVariantsThatPassedThreshold {
    
    publishDir "${publish_dir}", mode: 'copy', overwrite: true
    cpus 1
    memory '8 GB'
    queue "${queue}"
    
        
    input:
        path pickle from merged_reads_table_pickle
    
    output:
        path '*.csv' into variants_that_passed_threshold

    """
      
    python3 "${project_path}"/python/filterByThreshold.py \
            --in-pickle "${pickle}"  \
            --threshold ${threshold} \
            --positions `echo "${positions}"` \
            --out-csv 'variants_that_passed.csv'

    """
}  


