Sequence analysis pipeline written in Nextflow to process amplicon sequence data for Cas9-mutagenesis experiments using HPC-cluster (Slurm)

The pipeline include three Nextflow workflows (files):
- `trim_merge_map.nf` to process raw sequencing data returning alighned BAM files
- `determine_variants_above_threshould.nf` to detect sequence variants that are above a minimal read coverage (e.g. 100 reads)      
- `count_reads.nf` enumerate sequence variants a minimal read coverage and export as a csv file

## Dependencies

The pipline uses:
- FastQC
- cutadapt
- flash
- bwa
- samtools
- Python package: pysam
- Bioconducter package: Biostrings

## NGS processing steps

1.	Quality control of fastq files using FastQ 
2.	Demultiplexing raw sequencing reads (using barcodes) and trimming adapters with cutadapt 
3.	Merging paired reads using flash 
4.	Aligning reads to the reference sequence using bwa with high gap open penalties for deletions and insertions (-O 16,16). Sorting and indexing aligned reads files using samtools.
5.	Importing aligned reads into Python environment with the help of library pysam. Filtering out reads containing indels, calling unique variants and counting variant frequencies.  
6.	Discarding variants with the frequency of less than a specified threshold. Exporting the frequency table of variants for subsequent analysis.

## Usage 

1. Prepare input files in `in_data` directory:
    - Primer barcodes for demultiplexing samples in fasta format
    - Reference sequence of mutagenized gene (region) in fasta format
    - yaml parameter file specifying paths to directory(s) of input files, and other parameters as shown below. If you have multiple directories for input fastq files use shell expansion, i.e. dirList: 'raw_data/{folder1,another_folder}/':

    ```
    input_dir.yaml
    --------------
        dirList: 'raw_data/folder1/'
        publish_dir: 'out_data/before_selection'
        barcodes_path: 'in_data/barcodes.fasta'
        ref_fasta: "in_data/folA.fasta"
        FWD: "TCAGTCTGATTGCGGCGTTA"
        FWD_RC: "TAACGCCGCAATCAGACTGA"
        REV: "GGACGACCGATTGATTCCCA"
        REV_RC: "TGGGAATCAATCGGTCGTCC" 
        n_cores: 24
        flash: "flash/FLASH-1.2.11-Linux-x86_64/flash"
        fastqc: "shells/fastqc/FastQC/fastqc"
        fastqcall: false

    ``` 
        
2. Run the firts workflow:
```
nextflow run trim_merge_map.nf \
        --publish_dir PATH\   # path to a publish directory where output files will be copied
        --project_path PATH\  # path to prject directory
        --params-file input_dir.yaml
```

3. Run the second workflow:

```
nextflow run determine_variants_above_threshould.nf \
        --project_path PATH \   # path to project directory
        --threshold 100 \       # minimal detection threshold for sequence variants (number of merged reads)
        --bamdir PATH           # directory with BAM files

```


4. Run the third workflow

```
nextflow run count_reads.nf \
        --project_path PATH \  # path to project directory
        --bamdir  PATH         # directory with BAM files
        --variants_that_passed_threshold # file with varaints that passed threshold produced by running determine_variants_above_threshould.nf workflow
```
