{ pkgs ? import <nixpkgs> {} }:                                                           
                                                                                               
 let                                                                                           
   mach-nix = import (                                                                         
     builtins.fetchGit {                                                                       
       url = "https://github.com/DavHau/mach-nix";                                            
       ref = "refs/tags/3.5.0";                                                                          
     }) {                                                                                      
        python = "python39";                                                                    
     };                                                                                        
                                                                                               
   pythonPackages = mach-nix.mkPython {                                          
     requirements = ''                                                                         
       cutadapt
       pysam
       pandas                                                                                  
     '';                                                                                       
   };                                                                                          
                                                                                               
   RPackages = with pkgs; [                                                                    
     R                                                                                         
     rPackages.ggplot2                                                                         
     rPackages.plyr                                                                        
     rPackages.dada2                                                                       
     rPackages.argparse
     rPackages.GenomicAlignments
     rPackages.foreach
     rPackages.doParallel
     rPackages.stringr
     rPackages.data_table
                                                                          
   ];                                                                                          
                                                                                               
   linuxPackages = with pkgs; [                                                          
     samtools                                                                                   
     bwa                                                                                 
   ];                                                                                          
                                                                                               
 in                                                                                            
                                                                                               
 pkgs.mkShell {                                                                                
   buildInputs = [ pythonPackages ] ++ RPackages ++ linuxPackages;                             
 }
