#!/usr/bin/env Rscript
library("argparse")

  
parser <- ArgumentParser(description='Compare the number of variants in mutagenized position versus the rest of the reads by varying cut-off threshold')
parser$add_argument('--in-csv', dest='csv', metavar='FILE', type="character", 
                   help='input csv file with merged reads frequency')
parser$add_argument('--min', dest='min', help='minimal value of threshold',type="double",default=0)
parser$add_argument('--max', dest='max', help='maximal value of threshold',type="double",default=100)
parser$add_argument('--step', dest='step', help='step between min and max value',type="double",default=1)
parser$add_argument('--trim', dest='trim', help='trim reads from left and right',type="double",default=0)
parser$add_argument('--positions', dest='varposition', help='mutagenized positions relative to start of alignment',metavar='INT',type="integer",nargs='+',default=c(52:60))

parser$add_argument('--out-csv', dest='out', help='output file with a data frame of results',type="character",metavar='FILE')


args = parser$parse_args()

print(args)

suppressPackageStartupMessages(library(GenomicAlignments))
library(ggplot2)
library(data.table)
####################
### using blast
####################


#ref_fasta=args.ref

csv_file=args$csv
trim=args$trim
threshold_vector=seq(args$min,args$max,args$step)
variable_positions_to_keep=args$varposition
out=args$out




###############################
### functions
###############################



get_variable_positions= function(variant_vector){
    
    ## use consensus to find all variable regions max freq != sum
    variable_positions=which(
            apply(
        consensusMatrix(as(as.character(variant_vector),"DNAStringSet"))[c("A","C","T","G"),],
                    2,max) !=
            apply(
        consensusMatrix(as(as.character(variant_vector),"DNAStringSet"))[c("A","C","T","G"),],
                    2,sum)
                    )
    return(variable_positions)  
    }


#get_variable_positions(variant_vector)


get_unique_variants_outside_target = 
        function(vector_coverage_threshold, 
        variants_df,
        target_positions=variable_positions_to_keep,
        trim=0) {
            
            # determine length of Sv
            sv_length= as.integer(names(sort(table(nchar(paste(variants_df$sequence))),decreasing=T)[1]))
            
            n_variants_outside = 
                sapply(vector_coverage_threshold,function(thres){
        
                    sv_passed=paste(variants_df$sequence[variants_df$abundance>=thres])
                
                    masked=paste( substr(sv_passed,1+trim, min(target_positions)-1) , 
                                  substr(sv_passed,max(target_positions)+1,sv_length-trim), sep="")
                
                return( length(unique(masked)))
            })
                
            n_variants_inside = 
                sapply(vector_coverage_threshold,function(thres){
        
                    sv_passed=paste(variants_df$sequence[variants_df$abundance>=thres])
                
                    target=unique(substr(variants_df$sequence[variants_df$abundance>=thres],min(target_positions),max(target_positions)))
                   
                return( length(unique(target)))
            
            })
            
            combinded_res = rbind ( 
            data.frame( type = 'outside target',
                       n_varaints = n_variants_outside,
                       threshold=vector_coverage_threshold,
                       positions_considered=sv_length-trim*2-length(target_positions)),
            data.frame( type = 'inside target',
                       n_varaints = n_variants_inside,
                       threshold=vector_coverage_threshold,
                       positions_considered=length(target_positions)))
                       
            return(combinded_res)
            
        }





########################
# run
########################

## read file
sv=fread(csv_file,header=T)

## sort by abundance
sv=sv[order(sv$abundance,decreasing=T),]



data_to_return=get_unique_variants_outside_target(
        vector_coverage_threshold=threshold_vector, 
        variants_df=sv,
        target_positions=variable_positions_to_keep,
        trim=trim)


write.csv(data_to_return,file=out)



myplot=ggplot(data_to_return, aes(y=n_varaints,x=threshold,color=type,group=type))+
        geom_line()+
        scale_color_manual(values=c("red","blue"))+
        theme_bw()+
        scale_y_log10()+
        annotation_logticks(side = "l") 
        

pdf(file=gsub(".csv",".pdf",out),width=8, height=4)
print(myplot)
dev.off()

