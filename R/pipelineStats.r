#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)


# test if there is at least one argument: if not, return an error
if (length(args)==0) {
  stop("At least one argument must be supplied (input file).n", call.=FALSE)
} 
  




library(plyr)

mydata=read.table (args[1],header=F)
colnames(mydata)=c("step","file","n_reads")


mydata_ddply=ddply(mydata,.(step),summarise,Total=sum(n_reads))


mydata_ddply$Cumulative[1]=1
mydata_ddply$fromPreviousStep[1]=1

for (i in 2:nrow (mydata_ddply)){
    
    if (grepl("merged" ,mydata_ddply$step[i])) {  ## take into account merging
    
    mydata_ddply[i,"Cumulative"] = 2*mydata_ddply[i,"Total"]/mydata_ddply[1,"Total"]
    mydata_ddply[i,"fromPreviousStep"]= 2*mydata_ddply[i,"Total"]/(mydata_ddply[i-1,"Total"])
    
    } else {
    
    if (grepl("mapped" ,mydata_ddply$step[i])) {  ## take into account merging
    
    mydata_ddply[i,"Cumulative"] = 2*mydata_ddply[i,"Total"]/mydata_ddply[1,"Total"]
    mydata_ddply[i,"fromPreviousStep"]= mydata_ddply[i,"Total"]/mydata_ddply[i-1,"Total"]
    
    
    } else {  ## for all other steps
    
    mydata_ddply[i,"Cumulative"] = mydata_ddply[i,"Total"]/mydata_ddply[1,"Total"]
    mydata_ddply[i,"fromPreviousStep"]= mydata_ddply[i,"Total"]/mydata_ddply[i-1,"Total"]
    
    
    }
    
}}


    
print(mydata_ddply)


