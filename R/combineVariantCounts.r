#!/usr/bin/env Rscript
library("argparse")
suppressPackageStartupMessages(library(dada2))

parser <- ArgumentParser(description='Process some integers')

parser$add_argument('--in-csv', dest='counts_files', help='csv files with sequence variant counts',type="character",nargs='+',metavar='FILE')
parser$add_argument('--out-rds', dest='out', help='output path for RDS file containing a matrix of variant counts and samples',type="character",metavar='FILE')



args = parser$parse_args()

print(args)


####################
### using blast
####################

counts_files=args$counts_files
out=args$out



mylist=lapply(counts_files,function (S){ 
        data=read.csv(S,header=T)
        return(data)
})

print(counts_files)

names(mylist)=basename(counts_files)

seqtab=dada2::makeSequenceTable(samples=mylist)

str(seqtab)
dim(seqtab)

saveRDS(seqtab, file=out)
